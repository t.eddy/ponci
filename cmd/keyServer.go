/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"net/http"

	"gitlab.com/ponci-berlin/ponci/pkg/db"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/ponci-berlin/ponci/pkg/keyserver"
)

type keyServerConfig struct {
	Database  db.Config        `mapstructure:"database"`
	KeyServer keyserver.Config `mapstructure:"keyServer"`
	WebServer webConfig        `mapstructure:"webServer"`
}

var cmdKeyServer = &cobra.Command{
	Use:   "kms",
	Short: "Web service for collecting test data and issuing PoNCs",
	Long:  ``,
	RunE: func(cmd *cobra.Command, args []string) error {
		config := keyServerConfig{}
		err := viper.Unmarshal(&config)
		if err != nil {
			return err
		}
		dbHandle, err := db.OpenWithRetry(config.Database)
		if err != nil {
			return fmt.Errorf("couldn't connect to database: %w", err)
		}
		ks, err := keyserver.New(config.KeyServer, dbHandle)
		if err != nil {
			return err
		}
		router := gin.Default()
		router.POST("/", ks.HandleSubmitCert)
		router.GET("/healthz", ks.ServiceLiveness)
		if config.WebServer.ListenSocket == "" {
			config.WebServer.ListenSocket = "0.0.0.0:9090"
		}

		// Always publish the bundle at startup.
		ks.PublishBundle()
		return http.ListenAndServe(config.WebServer.ListenSocket, router)
	},
}
