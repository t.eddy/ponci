/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/ponci-berlin/ponci/pkg/db"
)

type databaseConfig struct {
	Database db.Config `mapstructure:"database"`
}

var cmdMigrate = &cobra.Command{
	Use:   "migrate",
	Short: "Migrate database tables.",
	Run: func(cmd *cobra.Command, args []string) {
		config := databaseConfig{}
		if err := viper.Unmarshal(&config); err != nil {
			panic(err)
		}
		dbHandle, err := db.OpenWithRetry(config.Database)
		if err != nil {
			panic(err)
		}
		if err := db.AutoMigrate(dbHandle); err != nil {
			panic(err)
		}
	},
}
