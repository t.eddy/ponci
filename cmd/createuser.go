/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	json "encoding/json"
	"errors"
	"log"

	"gitlab.com/ponci-berlin/ponci/pkg/credential"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/ponci-berlin/ponci/pkg/db"
	"gitlab.com/ponci-berlin/ponci/pkg/user"
)

var (
	vaccineAllowed     bool
	testAllowed        bool
	unspecifiedAllowed bool

	cmdCreateUser = &cobra.Command{
		Use:          "create-user",
		Short:        "This creates a new user.",
		Long:         `This creates a new user for an organisation.`,
		Args:         cobra.ExactArgs(1),
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			config := databaseConfig{}
			if err := viper.Unmarshal(&config); err != nil {
				return err
			}

			if !vaccineAllowed && !testAllowed && !unspecifiedAllowed {
				return errors.New("user needs to be allowed to create at least one kind of key")
			}

			allowedProcedures := make([]credential.Type, 0)
			if vaccineAllowed {
				allowedProcedures = append(allowedProcedures, credential.CredTypeVaccine)
			}

			if testAllowed {
				allowedProcedures = append(allowedProcedures, credential.CredTypeNegativeTest)
			}

			if unspecifiedAllowed {
				allowedProcedures = append(allowedProcedures, credential.CredTypeUnspecified)
			}

			dbHandle, err := db.OpenWithRetry(config.Database)
			if err != nil {
				return err
			}
			orgID := args[0]
			user, err := user.New(dbHandle, orgID, allowedProcedures)
			if err != nil {
				return err
			}

			jsonUser, err := json.Marshal(user)
			if err != nil {
				return err
			}

			log.Println(string(jsonUser))

			return nil
		},
	}
)

//nolint:gochecknoinits
func init() {
	cmdCreateUser.PersistentFlags().BoolVar(
		&vaccineAllowed,
		"vaccine-allowed",
		false,
		"Is the user allowed to create vaccine keys? (default: false)",
	)
	cmdCreateUser.PersistentFlags().BoolVar(
		&testAllowed,
		"test-allowed",
		false,
		"Is the user allowed to create test keys? (default: false)",
	)
	cmdCreateUser.PersistentFlags().BoolVar(
		&unspecifiedAllowed,
		"unspecified-allowed",
		false,
		"Is the user allowed to create unspecified (debug) keys? (default: false)",
	)
}
