/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"bytes"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"image"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/liyue201/goqr"

	"github.com/fxamacker/cbor/v2"
	"github.com/spf13/cobra"

	"github.com/PONCI-Berlin/go-cose"

	"gitlab.com/ponci-berlin/ponci/pkg/baercode"
	"gitlab.com/ponci-berlin/ponci/pkg/keymanagement"
)

const (
	defaultEnvironment = "dev"
)

var (
	imageMode   bool
	environment string
)

var environmentURLs = map[string]string{
	"prod": "https://s3-de-central.profitbricks.com:443/baercode/",
	"dev":  "https://s3-de-central.profitbricks.com:443/baercode-dev/",
}

var cmdVerify = &cobra.Command{
	Use:          "verify",
	Short:        "verify a baercode on cli",
	Long:         `parses and verifies a base64 encoded baercode against the production certificate bundle`,
	SilenceUsage: true,
	Args:         cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		if !verifyEnvironment(environment) {
			return fmt.Errorf("invalid environment: %s", environment)
		}

		code := args[len(args)-1]
		var bcb64 string
		var err error

		if imageMode {
			bcb64, err = recogniseFile(code)
			if err != nil {
				return err
			}
			log.Printf("Found QR contents: %s", bcb64)
		} else {
			bcb64 = code
		}
		bundle := downloadAndParseBundle()
		kid := parseBaercodeID(bcb64)

		var found bool

		for _, keys := range bundle.Keys {
			keySet := keys.ToKeySet()
			bid := keymanagement.NewKeyID(keySet.ECCKey)

			if bytes.Equal(kid, bid) {
				found = true
				creds, _, err := baercode.VerifyBaerCode(bcb64, keySet.ECCKey, keySet.AESKey)
				if err == nil {
					log.Println("success")
					log.Printf("%+v\n", creds)
					return nil
				}
				log.Println("signature invalid")
				log.Println(err.Error())
				log.Printf("%+v\n", creds)
			}
		}
		if !found {
			log.Println("key not found in bundle")
		}
		return errors.New("could not verify code")
	},
}

//nolint:gochecknoinits
func init() {
	envs := ""
	for k := range environmentURLs {
		envs += fmt.Sprintf("%s ", k)
	}
	cmdVerify.PersistentFlags().BoolVarP(
		&imageMode,
		"image-mode",
		"i",
		false,
		"Argument is the path to an image instead of a base64 baercode. Default: false",
	)
	cmdVerify.PersistentFlags().StringVarP(
		&environment,
		"environment",
		"e",
		defaultEnvironment,
		fmt.Sprintf("What environment's bundle to use. Choices: %s, Default: %s", envs, defaultEnvironment),
	)
}

func parseBaercodeID(bcb64 string) []byte {
	bcBytes, err := base64.StdEncoding.DecodeString(bcb64)
	if err != nil {
		log.Fatalln(err.Error())
	}
	bcBytes = bcBytes[2:]
	signMessage := &cose.SignMessage{}
	err = cbor.Unmarshal(bcBytes, signMessage)
	if err != nil {
		log.Fatalln(err.Error())
	}
	kid, _ := signMessage.Signatures[0].Headers.Unprotected[4].([]byte)
	log.Printf("baercode kid: %+v\n", hex.EncodeToString(kid))
	return kid
}

func downloadAndParseBundle() *keymanagement.Bundle {
	url := fmt.Sprintf("%sbundle.cose", environmentURLs[environment])
	resp, err := http.Get(url) //nolint:gosec
	if err != nil {
		log.Fatalln(err.Error())
	}
	bundleBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err.Error())
	}
	resp.Body.Close()

	signMessage := &cose.SignMessage{}
	err = cbor.Unmarshal(bundleBytes, signMessage)
	if err != nil {
		log.Fatalln(err.Error())
	}
	bundle := &keymanagement.Bundle{}
	err = cbor.Unmarshal(signMessage.Payload, bundle)
	if err != nil {
		log.Fatalln(err.Error())
	}

	return bundle
}

func recogniseFile(imgPath string) (string, error) {
	imgdata, err := ioutil.ReadFile(imgPath)
	if err != nil {
		return "", err
	}

	img, _, err := image.Decode(bytes.NewReader(imgdata))
	if err != nil {
		return "", err
	}

	qrCodes, err := goqr.Recognize(img)
	if err != nil {
		return "", err
	}

	if len(qrCodes) != 1 {
		return "", fmt.Errorf("found %d QR code(s), expected 1", len(qrCodes))
	}

	return string(qrCodes[0].Payload), nil
}

func verifyEnvironment(e string) bool {
	for k := range environmentURLs {
		if k == e {
			return true
		}
	}
	return false
}
