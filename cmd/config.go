/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"github.com/spf13/viper"
)

type webConfig struct {
	ListenSocket   string `mapstructure:"listenSocket"`
	PublicFilesDir string `mapstructure:"publicFilesDir"`
	PublicFilesURL string `mapstructure:"publicFilesURL"`
}

func setConfigDefaults() {
	// Database defaults
	viper.SetDefault("database.hostname", "localhost")
	viper.SetDefault("database.username", "baercode")
	viper.SetDefault("database.password", "")
	viper.SetDefault("database.dbName", "baercode")
	viper.SetDefault("database.port", 5432)
	viper.SetDefault("database.sslMode", true)

	// Issuer defaults
	viper.SetDefault("issuer.orgName", "BärCODE")
	viper.SetDefault("issuer.credentialType", 1)
	viper.SetDefault("issuer.userID", "baercode")
	viper.SetDefault("issuer.password", "")
	viper.SetDefault("issuer.otpSecret", "")
	viper.SetDefault("issuer.debug", false)
	viper.SetDefault("issuer.keyRotationTime", "02:02")
	viper.SetDefault("issuer.keyRotationPeriod", 24)
	viper.SetDefault("issuer.keyUpdatePollPeriod", 1)

	// KeyManager defaults
	viper.SetDefault("keyManagement.caUrl", "http://kms:9090")
	viper.SetDefault("keyManagement.debugFilePath", "")
	viper.SetDefault("keyManagement.debugFilePrefix", "debug_")
	viper.SetDefault("keyManagement.encryptionKeyName", "public/aes.hex")
	viper.SetDefault("keyManagement.orgName", "PONCI-Berlin")
	viper.SetDefault("keyManagement.saveFilepath", "ponc.save")
	viper.SetDefault("keyManagement.writeDebugFiles", false)

	// KeyServer defaults
	viper.SetDefault("keyServer.certBundleName", "public/cert.pem")

	// WebServer defaults
	viper.SetDefault("webServer.listenSocket", "") // default is set by the respective applications
	viper.SetDefault("webServer.publicFilesDir", "public/")
	viper.SetDefault("webServer.publicFilesURL", "/public/")
}
