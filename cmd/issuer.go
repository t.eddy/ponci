/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/ponci-berlin/ponci/pkg/issuer"
)

type issuerConfig struct {
	WebServer webConfig     `mapstructure:"webServer"`
	Issuer    issuer.Config `mapstructure:"issuer"`
}

var cmdIssuer = &cobra.Command{
	Use:   "issuer",
	Short: "Web service for collecting test data and issuing PoNCs",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		config := issuerConfig{}
		err := viper.Unmarshal(&config)
		if err != nil {
			log.Fatal("reading config file: " + err.Error())
		}
		issuer := issuer.New(config.Issuer)
		router := gin.Default()
		router.POST("/createCredential", issuer.HandleCreateCredential)
		router.GET("/", issuer.HandleGet)
		router.StaticFS("/public", http.Dir("public"))

		if config.WebServer.ListenSocket == "" {
			config.WebServer.ListenSocket = "0.0.0.0:8080"
		}

		log.Fatal(http.ListenAndServe(config.WebServer.ListenSocket, router))
	},
}
