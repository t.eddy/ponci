/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cose

import (
	"crypto/ecdsa"
	"crypto/rand"
	"fmt"

	"github.com/PONCI-Berlin/go-cose"
)

var eccBitsAlgMapping = map[int]*cose.Algorithm{
	521: cose.ES512,
	384: cose.ES384,
	256: cose.ES256,
}

type SignMessage struct {
	*cose.SignMessage
}

func getSignerFromECDSAPrivateKey(privateKey *ecdsa.PrivateKey, alg *cose.Algorithm) (*cose.Signer, error) {
	return cose.NewSignerFromKey(alg, privateKey)
}

// NewSignMessage creates a basic sign message with a payload.
func NewSignMessage(payload []byte) (msg *SignMessage) {
	msg = &SignMessage{cose.NewSignMessage()}
	msg.Payload = payload
	return
}

// SignMessageFromCBOR takes CBOR encoded data and returns a new SignMessage.
func SignMessageFromCBOR(encSignMessage []byte) (m *SignMessage, err error) {
	sm := &cose.SignMessage{}
	err = sm.UnmarshalCBOR(encSignMessage)
	if err != nil {
		return
	}
	m = &SignMessage{sm}
	return
}

func (sm *SignMessage) UnmarshalCBOR(data []byte) error {
	child, err := SignMessageFromCBOR(data)
	if err != nil {
		return err
	}
	sm.SignMessage = child.SignMessage
	return err
}

func (sm *SignMessage) addESSignature(kid []byte, alg *cose.Algorithm) {
	sig := cose.NewSignature()
	sig.Headers.Unprotected["kid"] = kid
	sig.Headers.Protected["alg"] = alg.Name
	sm.AddSignature(sig)
}

// SignWithESKey adds a signer with kid, and then signs the message with the ECDSA Key.
func (sm *SignMessage) SignWithESKey(privateKey *ecdsa.PrivateKey, kid []byte) (err error) {
	cParams := privateKey.Curve.Params()
	if _, ok := eccBitsAlgMapping[cParams.BitSize]; !ok {
		return fmt.Errorf("not supported ecc bitsize: %d", cParams.BitSize)
	}
	alg := eccBitsAlgMapping[cParams.BitSize]
	sm.addESSignature(kid, alg)
	signer, err := getSignerFromECDSAPrivateKey(privateKey, alg)
	if err != nil {
		return
	}
	return sm.Sign(rand.Reader, []byte(""), []cose.Signer{*signer})
}

// ESSignatureDigests returns the hashed signature digests of the message signatures.
// We take the public key of the (possibly unknown) private key for the curve params.
func (sm *SignMessage) ESSignatureDigests(publicKey *ecdsa.PublicKey, kid []byte) (digests [][]byte, err error) {
	alg, ok := eccBitsAlgMapping[publicKey.Curve.Params().BitSize]
	if !ok {
		return nil, fmt.Errorf("not supported ecc bitsize: %d", publicKey.Curve.Params().BitSize)
	}
	sm.addESSignature(kid, alg)
	return sm.SignatureDigests([]byte(""))
}

// VerifyWithESKey verifies if the SignMessage has been signed with the given key.
func (sm *SignMessage) VerifyWithESKey(publicKey *ecdsa.PublicKey) (err error) {
	cParams := publicKey.Curve.Params()
	if _, ok := eccBitsAlgMapping[cParams.BitSize]; !ok {
		return fmt.Errorf("not supported ecc bitsize: %d", cParams.BitSize)
	}
	alg := eccBitsAlgMapping[cParams.BitSize]
	signature := sm.Signatures[0]

	if signature.Headers.Protected[1] != alg.Value {
		return fmt.Errorf("algorithm not supported: %s", signature.Headers.Protected["alg"])
	}

	verifier := cose.Verifier{
		PublicKey: publicKey,
		Alg:       alg,
	}
	return sm.Verify([]byte(""), []cose.Verifier{verifier})
}
