/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cose

import (
	"errors"
	"fmt"
	"log"
	"reflect"

	"github.com/PONCI-Berlin/go-cose"
	"github.com/fxamacker/cbor/v2"

	"gitlab.com/ponci-berlin/ponci/pkg/aesgcm"
)

const (
	Encrypt0MessageTag = 16

	Encrypt0Context = "Encrypt0"

	coseAlgA128GCM = 1
	coseAlgA192GCM = 2
	coseAlgA256GCM = 3

	bitsInByte = 8

	coseHeaderIV = 5
)

var (
	AESGCMNumbers = map[int]int{
		128: coseAlgA128GCM,
		192: coseAlgA192GCM,
		256: coseAlgA256GCM,
	}

	ErrInvalidAESKeyLength = errors.New("invalid key size")
	ErrUnsupportedAlg      = errors.New("unsupported algorithm")
)

// Encrypt0Message represents an unencrypted Encrypt0Message.
type Encrypt0Message struct {
	Headers    *cose.Headers
	Payload    []byte
	Ciphertext []byte
}

func NewEncrypt0Message(payload []byte) (message *Encrypt0Message) {
	return &Encrypt0Message{
		Headers: &cose.Headers{
			Protected:   map[interface{}]interface{}{},
			Unprotected: map[interface{}]interface{}{},
		},
		Payload: payload,
	}
}

func (m *Encrypt0Message) AddKID(kid []byte) {
	m.Headers.Unprotected[4] = kid
}

func (m *Encrypt0Message) EncodeAESGCMMessage(key []byte) (err error) {
	bits := len(key) * bitsInByte
	algNumber, ok := AESGCMNumbers[bits]
	if !ok {
		return ErrInvalidAESKeyLength
	}
	m.Headers.Protected["alg"] = algNumber
	externalData := m.buildExternalData()
	ciphertext, nonce, err := aesgcm.Encrypt(m.Payload, key, externalData)
	if err != nil {
		return err
	}
	m.Headers.Unprotected[5] = nonce
	m.Ciphertext = ciphertext

	return nil
}

func (m *Encrypt0Message) DecodeAESGCMMessage(key []byte) (err error) {
	bits := len(key) * bitsInByte
	algNumber, ok := AESGCMNumbers[bits]
	if !ok {
		return ErrInvalidAESKeyLength
	}

	if m.Headers.Protected[1] != algNumber {
		return ErrUnsupportedAlg
	}

	// Messy way to get the IV
	var iv []byte
	for k, v := range m.Headers.Unprotected {
		if k.(uint64) == coseHeaderIV {
			iv, ok = v.([]byte)
			if !ok {
				return errors.New("iv was not of the right type")
			}
		}
	}

	if len(iv) == 0 {
		return errors.New("couldn't find nonce")
	}

	externalData := m.buildExternalData()

	plaintext, err := aesgcm.Decrypt(m.Ciphertext, key, iv, externalData)
	if err != nil {
		return
	}

	m.Payload = plaintext

	return
}

func (m *Encrypt0Message) buildExternalData() []byte {
	encStruct := []interface{}{
		Encrypt0Context,
		m.Headers.EncodeProtected(),
		[]byte(""),
	}

	encOpt := cbor.EncOptions{
		IndefLength: cbor.IndefLengthForbidden,
	}

	encMode, err := encOpt.EncMode()
	if err != nil {
		log.Fatalf("Couldn't initialise encoder: %s", err)
	}

	encodedEncStruct, err := encMode.Marshal(encStruct)
	if err != nil {
		log.Fatalf("Couldn't encode enc structure: %s", err)
	}

	return encodedEncStruct
}

type encrypt0Message struct {
	_           struct{} `cbor:",toarray"`
	Protected   []byte
	Unprotected map[interface{}]interface{}
	Ciphertext  []byte
}

func Encrypt0MessageFromCBOR(marshalled []byte) (message *Encrypt0Message, err error) {
	tags := cbor.NewTagSet()
	err = tags.Add(
		cbor.TagOptions{
			EncTag: cbor.EncTagRequired,
			DecTag: cbor.DecTagRequired,
		},
		reflect.TypeOf(Encrypt0Message{}),
		Encrypt0MessageTag,
	)

	if err != nil {
		return nil, err
	}

	decOpt := cbor.DecOptions{
		IndefLength: cbor.IndefLengthForbidden,
		IntDec:      cbor.IntDecConvertSigned,
	}

	decoder, err := decOpt.DecModeWithTags(tags)
	if err != nil {
		return nil, err
	}

	var raw cbor.RawTag
	err = decoder.Unmarshal(marshalled, &raw)
	if err != nil {
		return nil, err
	}

	if raw.Number != Encrypt0MessageTag {
		return nil, fmt.Errorf("cbor: wrong tag number: %d", raw.Number)
	}

	var m encrypt0Message
	err = cbor.Unmarshal(raw.Content, &m)
	if err != nil {
		return nil, err
	}

	msgHeaders := &cose.Headers{}
	err = msgHeaders.Decode([]interface{}{m.Protected, m.Unprotected})
	if err != nil {
		return nil, err
	}

	message = &Encrypt0Message{
		Headers:    msgHeaders,
		Ciphertext: m.Ciphertext,
	}

	return
}

func (m *Encrypt0Message) MarshalCBOR() (encoded []byte, err error) {
	if len(m.Ciphertext) == 0 {
		return nil, errors.New("refusing to marshal empty message")
	}

	encOpt := cbor.EncOptions{
		IndefLength: cbor.IndefLengthForbidden,
	}

	encMode, err := encOpt.EncMode()
	if err != nil {
		return nil, err
	}

	e := encrypt0Message{
		Protected:   m.Headers.EncodeProtected(),
		Unprotected: m.Headers.EncodeUnprotected(),
		Ciphertext:  m.Ciphertext,
	}

	return encMode.Marshal(cbor.Tag{Number: Encrypt0MessageTag, Content: e})
}
