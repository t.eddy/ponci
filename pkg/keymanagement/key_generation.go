package keymanagement

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"

	"gitlab.com/ponci-berlin/ponci/pkg/cose"
)

func RollKeys(
	writeDebugFiles bool,
	debugFilePath string,
	debugFilePrefix string,
) ([]byte, *ecdsa.PrivateKey, []byte, error) {
	newSignatureKey, err := generateSignatureKeys()
	if err != nil {
		log.Println(err.Error())
		return nil, nil, nil, err
	}
	newEncryptionKey, err := generateEncryptionKey()
	if err != nil {
		log.Fatalf("failed to generate aes key: %s\n", err.Error())
	}

	publicKey := &newSignatureKey.PublicKey

	ecCose, err := cose.MarshallECKey(publicKey)
	if err != nil {
		log.Fatal(err.Error())
	}

	if writeDebugFiles {
		err = WriteHexFile(ecCose, path.Join(debugFilePath, debugFilePrefix+"ecdsa-cose.hex"))
		if err != nil {
			return nil, nil, nil, err
		}
		err = WriteHexFile(newEncryptionKey, path.Join(debugFilePath, debugFilePrefix+"aes.hex"))
		if err != nil {
			return nil, nil, nil, err
		}
	}

	id := NewKeyID(publicKey)

	return id, newSignatureKey, newEncryptionKey, nil
}

func generateSignatureKeys() (*ecdsa.PrivateKey, error) {
	priv, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	if err != nil {
		log.Println(err.Error())
	}
	return priv, err
}

func generateEncryptionKey() ([]byte, error) {
	key := make([]byte, 16)
	var err error
	for i := 0; i < 5; i++ {
		_, err = rand.Read(key)
		if err == nil {
			break
		}
	}
	if err != nil {
		return nil, fmt.Errorf("reading random source: %w", err)
	}
	return key, nil
}

func WriteHexFile(key []byte, target string) error {
	keyString := hex.EncodeToString(key)
	return ioutil.WriteFile(target, []byte(keyString), os.ModePerm) // TODO handle err
}
