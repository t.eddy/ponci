/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package keymanagement

import (
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"log"
	rand2 "math/rand"
	"time"

	"gorm.io/gorm"

	"github.com/fxamacker/cbor/v2"
	"github.com/google/uuid"
	"gitlab.com/ponci-berlin/ponci/pkg/cose"
	"gitlab.com/ponci-berlin/ponci/pkg/credential"
)

type KeyManager struct {
	lastUpdated time.Time
	signingKey  *ecdsa.PrivateKey
	keyID       []byte
	rawCert     []byte
}

func New(keyPath string, certPath string) (*KeyManager, error) {
	ecdsaKey, err := readECKeyFromPem(keyPath)
	if err != nil {
		return nil, fmt.Errorf("couldn't parse ECDSA key %s: %w", keyPath, err)
	}

	rawCert, err := ioutil.ReadFile(certPath)
	if err != nil {
		return nil, fmt.Errorf("couldn't read certificate file %s: %w ", rawCert, err)
	}

	keyID := NewKeyID(&ecdsaKey.PublicKey)
	return &KeyManager{
		lastUpdated: time.Now(),
		signingKey:  ecdsaKey,
		keyID:       keyID,
		rawCert:     rawCert,
	}, nil
}

func readECKeyFromPem(keyPath string) (*ecdsa.PrivateKey, error) {
	bytes, err := ioutil.ReadFile(keyPath)
	if err != nil {
		return nil, fmt.Errorf("couldn't read key file at %s: %w", keyPath, err)
	}

	block, rest := pem.Decode(bytes)
	if block == nil {
		return nil, fmt.Errorf("couldn't PEM decode %s, rest bytes: %s", keyPath, rest)
	}
	return x509.ParseECPrivateKey(block.Bytes)
}

func (km *KeyManager) GenerateSignedBundle(dbHandle *gorm.DB) ([]byte, error) {
	keySets, err := GetValidKeySets(dbHandle)
	if err != nil {
		return nil, fmt.Errorf("could not get valid keysets: %w", err)
	}

	return GenerateSignedBundle(keySets, km.GetSigningKey(), km.GetKeyID())
}

func (km *KeyManager) GetSigningKey() *ecdsa.PrivateKey {
	return km.signingKey
}

func (km *KeyManager) GetRawCert() []byte {
	return km.rawCert
}

func (km *KeyManager) GetKeyID() []byte {
	return km.keyID
}

func (km *KeyManager) SetKeyID(id []byte) {
	km.keyID = id
}

func NewKeyID(pub *ecdsa.PublicKey) []byte {
	num := pub.X.Bytes()
	return num[len(num)-16:]
}

func NewRandomString() []byte {
	id, err := uuid.NewRandom()
	if err != nil {
		log.Println(err.Error())
		badSrc := rand2.NewSource(time.Now().UnixNano())
		uuid.SetRand(rand2.New(badSrc)) //nolint:gosec
		// NewRandom only throws errors if the random source returns an error,
		// and the math/rand prng always returns a nil error
		id, _ = uuid.NewRandom()
	}
	idBytes, _ := id.MarshalBinary() // MarshalBinary always returns a nil error
	return idBytes
}

type SubmitKeysRequest struct {
	CredType credential.Type
	AesKey   cbor.RawMessage
	ECCKey   cbor.RawMessage
}

func parseSubmitKeysRequest(data []byte) (aesKey []byte, eccKey *ecdsa.PublicKey, credType credential.Type, err error) {
	req := SubmitKeysRequest{}
	err = cbor.Unmarshal(data, &req)
	if err != nil {
		return nil, nil, credential.CredTypeUnspecified, fmt.Errorf("parsing cbor request: %w", err)
	}

	aesKey, err = cose.UnMarshallAESKey(req.AesKey)
	if err != nil {
		return nil, nil, credential.CredTypeUnspecified, fmt.Errorf("parsing aes cose key: %w", err)
	}

	eccKey, err = cose.UnMarshallECKey(req.ECCKey)
	if err != nil {
		return nil, nil, credential.CredTypeUnspecified, fmt.Errorf("parsing ecc cose key: %w", err)
	}

	credType = req.CredType

	return
}

func ParseSubmitKeysRequest(reqBytes []byte, orgID string) (KeySet, error) {
	aesKey, eccKey, credType, err := parseSubmitKeysRequest(reqBytes)
	if err != nil {
		return KeySet{}, err
	}

	return KeySet{
		CredType: credType,
		AESKey:   aesKey,
		ECCKey:   eccKey,
		OrgID:    orgID,
	}, nil
}
