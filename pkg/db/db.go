/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package db

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/ponci-berlin/ponci/pkg/bitset"

	"gitlab.com/ponci-berlin/ponci/pkg/keymanagement"

	"github.com/cenkalti/backoff/v4"

	"gitlab.com/ponci-berlin/ponci/pkg/user"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Config struct {
	Hostname    string `mapstructure:"hostname"`
	Username    string `mapstructure:"username"`
	Password    string `mapstructure:"password"`
	DBName      string `mapstructure:"dbName"`
	Port        int    `mapstructure:"port"`
	SSLMode     bool   `mapstructure:"sslMode"`
	SSLRootCert string `mapstructure:"sslRootCert"`
}

// Open opens a new database connection based on the given configuration.
func Open(config Config) (dbHandle *gorm.DB, err error) {
	sslMode := "disable"
	rootCert := ""
	if config.SSLMode {
		sslMode = "require"
		if config.SSLRootCert != "" {
			rootCert = fmt.Sprintf("sslrootcert=%s", config.SSLRootCert)
		}
	}

	dsn := fmt.Sprintf(
		"host=%s user=%s  password=%s dbname=%s port=%d sslmode=%s TimeZone=%s %s",
		config.Hostname,
		config.Username,
		config.Password,
		config.DBName,
		config.Port,
		sslMode,
		time.UTC,
		rootCert,
	)
	return gorm.Open(postgres.Open(dsn), &gorm.Config{})
}

// OpenWithRetry opens a new database connection based on the given configuration.
// It will retry this with an exponential backoff.
func OpenWithRetry(config Config) (dbHandle *gorm.DB, err error) {
	dbOpen := func() error {
		log.Printf("Connecting to database %s on %s:%d", config.DBName, config.Hostname, config.Port)
		dbHandle, err = Open(config)
		if err != nil {
			return err
		}
		return nil
	}

	err = backoff.Retry(dbOpen, backoff.NewExponentialBackOff())
	if err != nil {
		return nil, err
	}

	return dbHandle, nil
}

// AutoMigrate auto migrates the required models.
func AutoMigrate(dbHandle *gorm.DB) (err error) {
	migrator := dbHandle.Migrator()
	if migrator.HasColumn(&user.User{}, "procedure_id") {
		err := migrator.RenameColumn(&user.User{}, "procedure_id", "procedure_set")
		if err != nil {
			return err
		}
		var users []user.User
		dbHandle.Find(&users)
		for i := range users {
			procedureSet := bitset.Bits(0)
			credType := users[i].ProcedureSet
			users[i].ProcedureSet = int(bitset.Set(procedureSet, uint(credType)))
			if err := dbHandle.Save(&users[i]).Error; err != nil {
				log.Printf("Failed to convert procedure %d for %s: %s", credType, users[i].UserID, err)
				continue
			}
			log.Printf(
				"Converted procedure type %d to procedure set %d for user %s",
				users[i].ProcedureSet,
				credType,
				users[i].UserID,
			)
		}
	}
	return dbHandle.AutoMigrate(&user.User{}, &keymanagement.KeyRecord{})
}
