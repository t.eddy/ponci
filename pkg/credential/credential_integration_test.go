// +build integration

/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package credential_test

import (
	"encoding/hex"
	"os"
	"path"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ponci-berlin/ponci/pkg/credential"
)

const (
	credentialFile = "php_credential.cbor"
	scratchDirKey  = "SCRATCH_DIR"
)

func TestDecodeCredentialIntegration(t *testing.T) {
	scratchDir := os.Getenv(scratchDirKey)
	if scratchDir == "" {
		t.Fatalf("not running without SCRATCH_DIR set")
	}

	procedures := []credential.Procedure{
		{
			Type: 1,
			Time: time.Date(2021, time.May, 1, 8, 0, 0, 0, time.UTC),
		},
	}

	testData := credential.Credential{
		FirstName:         "Max",
		LastName:          "Mustermann",
		DateOfBirth:       time.Date(1990, time.April, 1, 00, 00, 00, 00, time.UTC),
		Procedures:        procedures,
		ProcedureOperator: "PoNC Inc",
		ProcedureResult:   false,
	}

	credentialPath := path.Join(scratchDir, credentialFile)

	contents, err := os.ReadFile(credentialPath)
	if err != nil {
		t.Fatalf("couldn't read credential file %s: %s", credentialPath, err)
	}

	rawCBOR := make([]byte, hex.DecodedLen(len(contents)))
	_, err = hex.Decode(rawCBOR, contents)
	if err != nil {
		t.Fatalf("couldn't decode hex %s: %s", contents, err)

	}
	cred, err := credential.FromCBOR(rawCBOR)
	if err != nil {
		t.Fatalf("couldn't decode CBOR: %s", err)
	}

	assert.Equal(t, testData.FirstName, cred.FirstName)
	assert.Equal(t, testData.LastName, cred.LastName)
	assert.Equal(t, testData.DateOfBirth.UTC(), cred.DateOfBirth.UTC())
	assert.Equal(t, testData.Procedures[0].Type, cred.Procedures[0].Type)
	assert.Equal(t, testData.Procedures[0].Time.UTC(), cred.Procedures[0].Time.UTC())
	assert.Equal(t, testData.ProcedureOperator, cred.ProcedureOperator)
	assert.Equal(t, testData.ProcedureResult, cred.ProcedureResult)
}
