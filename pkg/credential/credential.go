/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package credential

import (
	"time"

	"github.com/fxamacker/cbor/v2"
)

// Credential represents the bare minimum data the verifier needs to both verify that the offered
// test is valid, and that the valid test belongs to the person offering it.
type Credential struct {
	_                 struct{} `cbor:",toarray"`
	FirstName         string
	LastName          string
	DateOfBirth       time.Time
	DiseaseType       DiseaseType
	Procedures        []Procedure
	ProcedureOperator string
	ProcedureResult   bool
}

// Procedure represents a procedure, which is a combination of procedure type and procedure time.
type Procedure struct {
	_    struct{}      `cbor:",toarray"`
	Type ProcedureType `json:"Type"`
	Time time.Time     `json:"Time"`
}

type Type int
type ProcedureType int
type DiseaseType int

const (
	CredTypeUnspecified Type = iota
	CredTypeVaccine
	CredTypeNegativeTest
	// CredTypeInvalid should remain the max value,
	// and is used for validating input values are within
	// the valid range of "0 up to but not including invalid".
	CredTypeInvalid
)

const (
	// ProcedureTypeNA - Not Applicable, used for tests and single shot vaccines.
	ProcedureTypeNA ProcedureType = iota
	// ProcedureTypeTestAntigen - Antigen tests.
	ProcedureTypeTestAntigen
	// ProcedureTypePCR - PCR tests.
	ProcedureTypePCR
	// ProcedureTypeCormirnaty - BioNTech Manufacturing GmbH - mRNA vaccine - Dual shots required.
	ProcedureTypeCormirnaty
	// ProcedureTypeJanssen - Janssen-Cilag International NV - Vector vaccine - Single shot required.
	ProcedureTypeJanssen
	// ProcedureTypeModerna - Moderna Biotech Spain, S.L. - mRNA vaccine - Dual shots required.
	ProcedureTypeModerna
	// ProcedureTypeVaxzevria - AstraZeneca AB, Sweden - Vector vaccine - Dual shots required.
	ProcedureTypeVaxzevria
)

const (
	// DiseaseTypeUnspecified - Unspecified disease.
	DiseaseTypeUnspecified DiseaseType = iota
	// DiseaseTypeCOVID19 - SARS-CoV-19.
	DiseaseTypeCOVID19
)

var CredTypeToProcedure = map[Type][]ProcedureType{
	CredTypeVaccine: {
		ProcedureTypeCormirnaty,
		ProcedureTypeJanssen,
		ProcedureTypeModerna,
		ProcedureTypeVaxzevria,
	},
	CredTypeNegativeTest: {
		ProcedureTypeTestAntigen,
		ProcedureTypePCR,
	},
}

// New takes all information and returns a credential.
func New(
	firstName string,
	lastName string,
	dob time.Time,
	procedures []Procedure,
	operator string,
	result bool,
) (cred Credential) {
	return Credential{
		FirstName:   firstName,
		LastName:    lastName,
		DateOfBirth: dob,
		// COVID is the only disease we support right now.
		DiseaseType:       DiseaseTypeCOVID19,
		Procedures:        procedures,
		ProcedureOperator: operator,
		ProcedureResult:   result,
	}
}

// FromCBOR decodes a CBOR encoded credential and returns it.
func FromCBOR(cborCredential []byte) (credential Credential, err error) {
	err = cbor.Unmarshal(cborCredential, &credential)
	return
}

// ToCBOR marshals the credential to CBOR.
func (c Credential) ToCBOR() (cborCredential []byte, err error) {
	return cbor.Marshal(c)
}
