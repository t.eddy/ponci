package bitset

// Bits represents an 8 bit bitset.
type Bits uint32

// Set sets the bit for the specific flag on the given mask and returns the new mask.
func Set(b Bits, flag uint) Bits { return b | (1 << flag) }

// Clear clears the bit for the specific flag.
func Clear(b Bits, flag uint) Bits { return b &^ (1 << flag) }

// Toggle toggles the bit for the credType.
func Toggle(b Bits, flag uint) Bits { return b ^ (1 << flag) }

// Has will return true if the bit is set, false if not.
func Has(b Bits, flag uint) bool { return b&(1<<flag) != 0 }
