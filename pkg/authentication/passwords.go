/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package authentication

import (
	"crypto/rand"
	"crypto/subtle"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"sync"

	"golang.org/x/crypto/argon2"
)

var (
	ErrMismatchedHashAndPassword = errors.New("ponc/authentication: hashedPassword is not the hash of the given password")
)

const (
	argonTime    = 1
	argonMemory  = 1024 * 1024 // 1GiB (base units is KiB)
	argonThreads = 2
	argonKeyLen  = 32
)

type hashLock struct {
	sync.Mutex
}

type ArgonConfig struct {
	Time    uint32
	Memory  uint32
	Threads uint8
	KeyLen  uint32
}

var DefaultPasswordConfig = ArgonConfig{
	Time:    argonTime,
	Memory:  argonMemory,
	Threads: argonThreads,
	KeyLen:  argonKeyLen,
}

var once sync.Once

var hl *hashLock

// using spaces as delimiters because it works nicely with fmt.Sscanf.
var storageFormat = "argon2id v=%d m=%d t=%d p=%d %s %s"

func newHashLock() *hashLock {
	once.Do(func() {
		hl = &hashLock{}
	})
	return hl
}

func (h *hashLock) IDKey(password, salt []byte, time, memory uint32, threads uint8, keyLen uint32) []byte {
	h.Lock()
	defer h.Unlock()
	return argon2.IDKey(password, salt, time, memory, threads, keyLen)
}

// GenerateHash is used to generate a new password hash for storing and
// comparing at a later date.
// randSrc should be nil for normal use and set to a math/rand.Reader for testing.
func GenerateHash(c ArgonConfig, password []byte, randSrc io.Reader) (string, error) {
	if randSrc == nil {
		randSrc = rand.Reader
	}

	salt := make([]byte, 16)
	if _, err := randSrc.Read(salt); err != nil {
		return "", err
	}

	h := newHashLock()
	hash := h.IDKey(password, salt, c.Time, c.Memory, c.Threads, c.KeyLen)
	b64Salt := base64.RawStdEncoding.EncodeToString(salt)
	b64Hash := base64.RawStdEncoding.EncodeToString(hash)
	return fmt.Sprintf(storageFormat, argon2.Version, c.Memory, c.Time, c.Threads, b64Salt, b64Hash), nil
}

// CompareHashAndPassword is used to compare a user-inputted password to a hash to see if the password matches.
func CompareHashAndPassword(hashData string, password []byte) error {
	c := &ArgonConfig{}
	var argonV int
	var b64salt, b64hash string
	_, err := fmt.Sscanf(hashData, storageFormat, &argonV, &c.Memory, &c.Time, &c.Threads, &b64salt, &b64hash)
	if err != nil {
		return fmt.Errorf("parsing hashData: %w", err)
	}

	salt, err := base64.RawStdEncoding.DecodeString(b64salt)
	if err != nil {
		return fmt.Errorf("decoding salt: %w", err)
	}

	decodedHash, err := base64.RawStdEncoding.DecodeString(b64hash)
	if err != nil {
		return fmt.Errorf("decoding hash: %w", err)
	}
	c.KeyLen = uint32(len(decodedHash))

	h := newHashLock()
	comparisonHash := h.IDKey(password, salt, c.Time, c.Memory, c.Threads, c.KeyLen)

	if subtle.ConstantTimeCompare(decodedHash, comparisonHash) == 1 {
		return nil
	}

	return ErrMismatchedHashAndPassword
}
