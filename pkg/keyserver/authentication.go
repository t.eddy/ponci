/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package keyserver

import (
	"errors"
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/ponci-berlin/ponci/pkg/authentication"
	"gitlab.com/ponci-berlin/ponci/pkg/user"
	"gorm.io/gorm"
)

const (
	elementsInAuthHeader = 3
)

// TODO we need to log more info on errors here to assist in threat detection.
func authenticateRequest(dbHandle *gorm.DB, ctx *gin.Context) (bool, *user.User) {
	authHeader := ctx.GetHeader("AUTHORIZATION")
	if authHeader == "" {
		ctx.Writer.WriteHeader(http.StatusUnauthorized)
		return false, nil
	}

	parts := strings.Split(authHeader, ":")
	if len(parts) != elementsInAuthHeader {
		ctx.Writer.WriteHeader(http.StatusUnauthorized)
		log.Printf("malformed headers")
		return false, nil
	}

	u, err := user.Fetch(dbHandle, parts[0])
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			ctx.Writer.WriteHeader(http.StatusUnauthorized)
			log.Printf("user not found %s\n", parts[0])
		} else {
			ctx.Writer.WriteHeader(http.StatusInternalServerError)
			log.Println(err.Error())
		}

		return false, nil
	}

	err = authentication.CompareHashAndPassword(u.Password, []byte(parts[1]))
	if err != nil {
		ctx.Writer.WriteHeader(http.StatusUnauthorized)
		_, writeErr := ctx.Writer.WriteString("incorrect username or password")
		log.Println(err.Error())
		if writeErr != nil {
			log.Printf("Also encountered an error while writing response: %s\n", writeErr)
		}
		return false, nil
	}

	if !authentication.ValidateOTP(u.OTPSecret, parts[2]) {
		ctx.Writer.WriteHeader(http.StatusUnauthorized)
		return false, nil
	}

	return true, u
}
