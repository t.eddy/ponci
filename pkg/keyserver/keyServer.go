/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package keyserver

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/ponci-berlin/ponci/pkg/credential"
	"gitlab.com/ponci-berlin/ponci/pkg/keymanagement"
	"gorm.io/gorm"
)

const (
	// The keyserver doesn't care about when a key is valid, but this can be given to the verifier as a hint.
	validityStart = 12 * time.Hour
	// Vaccine keys should be valid for two years, this should be revised in 2022 with existing records updated.
	vaccineValidity = 24 * time.Hour * 730
	// Test should be "valid" for 36 hours.
	debugValidity = 36 * time.Hour
	// 12-24 hour validity delay + 3 days of use + 3 days of validity of PCR tests +
	// half a day of margin for errors etc = 7.5 days.
	testValidity = 180 * time.Hour
)

type KeyServer struct {
	Keyman *keymanagement.KeyManager
	Config
	DB         *gorm.DB
	updateChan chan bool
}

type Config struct {
	CertBundleName     string        `mapstructure:"certBundleName"`
	PublishRefreshTime time.Duration `mapstructure:"publishRefreshTime"`
	CertName           string        `mapstructure:"certName"`
	S3Endpoint         string        `mapstructure:"s3Endpoint"`
	S3BucketName       string        `mapstructure:"s3BucketName"`
	S3AccessKeyID      string        `mapstructure:"s3AccessKeyID"`
	S3SecretAccessKey  string        `mapstructure:"s3SecretAccessKey"`
	S3UseSSL           bool          `mapstructure:"s3UseSSL"`
	SigningCert        string        `mapstructure:"signingCert"`
	SigningKey         string        `mapstructure:"signingKey"`
}

func New(config Config, dbHandle *gorm.DB) (*KeyServer, error) {
	updateChan := make(chan bool)
	keyman, err := keymanagement.New(config.SigningKey, config.SigningCert)
	if err != nil {
		return nil, err
	}
	ks := &KeyServer{keyman, config, dbHandle, updateChan}
	go ks.publishMonitor(config.PublishRefreshTime)
	return ks, nil
}

func (ks *KeyServer) HandleSubmitCert(ctx *gin.Context) {
	// If this fails we have bigger problems.
	defer ctx.Request.Body.Close()

	log.Println("Handling Submit!")
	// authenticate requester
	authenticated, user := authenticateRequest(ks.DB, ctx)
	if !authenticated {
		log.Println("couldn't authenticate request.")
		return
	}

	reqBytes, err := ioutil.ReadAll(ctx.Request.Body)
	if writeError(ctx, err, http.StatusBadRequest, "Unable to read request body") {
		return
	}

	keySet, err := keymanagement.ParseSubmitKeysRequest(reqBytes, user.OrgID)
	if writeError(ctx, err, http.StatusBadRequest, "Unable to parse request") {
		return
	}

	if !user.ProcedureTypeAllowed(keySet.CredType) {
		_ = writeError(
			ctx,
			fmt.Errorf("user %s not allowed to create keyset of type %d", user.UserID, keySet.CredType),
			http.StatusUnauthorized,
			"Not allowed to create this type of keyset",
		)
		return
	}

	keySet.ValidFrom, keySet.ValidTo = getKeySetValidity(keySet.CredType)
	log.Printf("Saving %+v\n", keySet)
	err = keySet.Save(ks.DB)
	if writeError(ctx, err, http.StatusInternalServerError, "internal server error") {
		return
	}

	id := keymanagement.NewKeyID(keySet.ECCKey)

	ctx.Writer.WriteHeader(http.StatusOK)
	_, err = ctx.Writer.Write(id)
	if err != nil {
		log.Printf("problem writing ID to client: %s", err)
	}

	ks.updateChan <- true
}

// ServiceLiveness is an endpoint to see if the service is alive.
// Just returns a 200 for now, but once we have a lightweight way of checking if the service is
// working properly, we should add it here.
func (ks *KeyServer) ServiceLiveness(ctx *gin.Context) {
	defer ctx.Request.Body.Close()

	ctx.Writer.WriteHeader(http.StatusOK)
	_, err := ctx.Writer.Write([]byte("ok"))
	if err != nil {
		log.Printf("Couldn't write to client liveness check: %s", err)
	}
}

// PublishBundle starts the publish process.
func (ks *KeyServer) PublishBundle() {
	ks.updateChan <- true
}

func (ks *KeyServer) publishMonitor(refreshTime time.Duration) {
	log.Println("Starting refresh ticker")
	refresh := time.NewTimer(refreshTime)
	log.Println("Starting to monitor the update channel.")
	// TODO: Add some logic here to make sure we don't update too often.
	for {
		select {
		case <-ks.updateChan:
			if err := publishBundle(ks); err != nil {
				log.Printf("error while publishing: %s", err)
			}
			refresh.Stop()
			refresh.Reset(refreshTime)
		// AFAIK you can't use two channels in a case statement.
		case <-refresh.C:
			log.Println("starting period refresh publishing.")
			if err := publishBundle(ks); err != nil {
				log.Printf("error while publishing: %s", err)
			}
			refresh.Stop()
			refresh.Reset(refreshTime)
		}
	}
}

func getKeySetValidity(credType credential.Type) (validFrom time.Time, validTo time.Time) {
	now := time.Now().UTC()
	validFrom = now.Add(validityStart)
	switch credType {
	case credential.CredTypeNegativeTest:
		validTo = now.Add(testValidity)
	case credential.CredTypeVaccine:
		validTo = now.Add(vaccineValidity)
	case credential.CredTypeUnspecified:
		validTo = now.Add(debugValidity)
	case credential.CredTypeInvalid:
		validFrom = now
		validTo = now
	default:
		validFrom = now
		validTo = now
	}

	return validFrom, validTo
}

func writeError(ctx *gin.Context, err error, status int, message string) bool {
	if err == nil {
		return false
	}
	log.Printf("%s: %s", message, err)
	ctx.Writer.WriteHeader(status)
	_, writeErr := ctx.Writer.WriteString(message)
	if writeErr != nil {
		log.Printf("Also encountered an error while writing to client: %s", writeErr)
	}
	return true
}
