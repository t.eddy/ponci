/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package keyserver

import (
	"bytes"
	"context"
	"fmt"
	"log"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

const (
	cborContentType = "application/cbor"
	certContentType = "application/x-x509-ca-cert"
)

type uploadOpts struct {
	Endpoint        string
	BucketName      string
	AccessKeyID     string
	SecretAccessKey string
	UseSSL          bool
}

func publishBundle(ks *KeyServer) error {
	// TODO: Maybe useful to check if the published bundle is the same?
	uOpts := uploadOpts{
		Endpoint:        ks.S3Endpoint,
		BucketName:      ks.S3BucketName,
		AccessKeyID:     ks.S3AccessKeyID,
		SecretAccessKey: ks.S3SecretAccessKey,
		UseSSL:          ks.S3UseSSL,
	}
	signedBundle, err := ks.Keyman.GenerateSignedBundle(ks.DB)
	if err != nil {
		return fmt.Errorf("couldn't generate signing bundle: %w", err)
	}

	if err := s3Upload(ks.CertName, ks.Keyman.GetRawCert(), certContentType, uOpts); err != nil {
		return fmt.Errorf("error publishing public key: %w", err)
	}

	if err := s3Upload(ks.CertBundleName, signedBundle, cborContentType, uOpts); err != nil {
		return fmt.Errorf("error uploading bundle: %w", err)
	}

	return nil
}

func s3Upload(objectName string, objectContent []byte, contentType string, opts uploadOpts) error {
	ctx := context.Background()
	log.Printf("Starting upload to s3://%s/%s/%s", opts.Endpoint, opts.BucketName, objectName)

	minioClient, err := minio.New(opts.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(opts.AccessKeyID, opts.SecretAccessKey, ""),
		Secure: opts.UseSSL,
	})
	if err != nil {
		return fmt.Errorf("couldn't initialise minio client: %w", err)
	}
	r := bytes.NewReader(objectContent)
	_, err = minioClient.PutObject(ctx, opts.BucketName, objectName, r, int64(len(objectContent)), minio.PutObjectOptions{
		ContentType: contentType,
		// This forces the object to be public.
		UserMetadata: map[string]string{"x-amz-acl": "public-read"},
		CacheControl: "no-store, max-age=0",
	})

	if err != nil {
		return fmt.Errorf("couldn't upload s3://%s/%s/%s: %w", opts.Endpoint, opts.BucketName, objectName, err)
	}

	log.Printf("Successful upload to s3://%s/%s/%s", opts.Endpoint, opts.BucketName, objectName)
	return nil
}
