/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package issuer

import (
	"encoding/base64"
	"encoding/hex"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/ponci-berlin/ponci/pkg/baercode"
	"gitlab.com/ponci-berlin/ponci/pkg/credential"
)

type createRequest struct {
	FirstName         string
	LastName          string
	DateOfBirth       time.Time              // RFC3339 date in UTC timezone
	Procedures        []credential.Procedure `json:"Procedures"`
	ProcedureOperator string
}

func (is *Issuer) HandleGet(ctx *gin.Context) {
	http.ServeFile(ctx.Writer, ctx.Request, "public/index-issuer.html") // TODO configurable
}

func (is *Issuer) HandleCreateCredential(ctx *gin.Context) {
	req := createRequest{}
	err := ctx.BindJSON(&req)
	if err != nil {
		ctxErr := ctx.Error(err)
		if ctxErr != nil {
			log.Printf("error during credential creation: %s", err)
			log.Printf("there was also an error while pushing the error to the context: %s", ctxErr)
		}
		return
	}

	// Cormirnaty is our first vaccine, everything after that should be a vaccine (for now).
	procedureResult := req.Procedures[0].Type >= credential.ProcedureTypeCormirnaty

	baercode := is.Issue(req.FirstName, req.LastName, req.DateOfBirth, req.Procedures, procedureResult)
	b64bc := base64.StdEncoding.EncodeToString(baercode)
	ctx.Writer.WriteHeader(http.StatusOK)
	if _, err := ctx.Writer.WriteString(b64bc); err != nil {
		log.Printf("error writing bärCODE to client: %s", err)
	}
}

// Issue generates a new BärCODE as a bitmap and returns it
// DoB will contain only date information with everything else being 0'd (including TZ, so that it uses UTC)
// This could potentially take a credential object containing the same fields.
func (is *Issuer) Issue(
	firstName string,
	lastName string,
	dob time.Time,
	procedures []credential.Procedure,
	result bool,
) []byte {
	keySet := is.GetCurrentKey()
	log.Println("using key: " + hex.EncodeToString(keySet.AESKey))
	data, err := baercode.CreateBaerCodeQR(
		firstName,
		lastName,
		dob,
		procedures,
		is.OrgName,
		result,
		keySet.Id,
		keySet.ECCKey.PrivateKey,
		keySet.AESKey,
	)
	if err != nil {
		log.Fatal(err.Error()) // TODO handle err
	}
	return data
}
