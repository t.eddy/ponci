/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package issuer

import (
	"fmt"
	"time"

	"gitlab.com/ponci-berlin/ponci/pkg/credential"
)

type KeySet struct {
	Id        []byte //nolint:golint,revive,stylecheck
	CredType  credential.Type
	AESKey    []byte
	ECCKey    *privateKey
	ValidFrom time.Time
	ValidTo   time.Time
}

func NewKeySet(credType credential.Type) (*KeySet, error) {
	newSignatureKey, err := generateSignatureKeys()
	if err != nil {
		return nil, fmt.Errorf("generating ec key: %w", err)
	}
	newEncryptionKey, err := generateEncryptionKey()
	if err != nil {
		return nil, fmt.Errorf("generating aes key: %w", err)
	}

	ks := &KeySet{
		Id:       newSignatureKey.getID(),
		CredType: credType,
		AESKey:   newEncryptionKey,
		ECCKey:   newSignatureKey,
	}
	return ks, nil
}
