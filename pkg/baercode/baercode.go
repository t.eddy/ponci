/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package baercode

import (
	"crypto/ecdsa"
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"time"

	"github.com/fxamacker/cbor/v2"

	"gitlab.com/ponci-berlin/ponci/pkg/cose"
	"gitlab.com/ponci-berlin/ponci/pkg/credential"
	"gitlab.com/ponci-berlin/ponci/pkg/qr"
)

const (
	// Version signifies the version of the BaerCode. Using an uint16 to give as more than
	// enough space to play with.
	Version         uint16 = 1
	VersionByteSize int    = 2

	DefaultQRScale int = 300
)

func addVersion(bytes []byte) (versionedBytes []byte) {
	versionBytes := make([]byte, VersionByteSize)
	binary.LittleEndian.PutUint16(versionBytes, Version)
	versionedBytes = make([]byte, 0)
	versionedBytes = append(versionedBytes, versionBytes...)
	versionedBytes = append(versionedBytes, bytes...)
	return versionedBytes
}

func getVersion(bytes []byte) (version uint16, rest []byte) {
	versionBytes := bytes[:VersionByteSize]
	rest = bytes[VersionByteSize:]
	version = binary.LittleEndian.Uint16(versionBytes)
	return
}

// CreateBaerCodeQR creates the credential, cbors it, gzips it, encrypts it, signs it, base64s it,
// encodes it in a QR code and returns the base64ed image.
func CreateBaerCodeQR(
	firstName string,
	lastName string,
	dob time.Time,
	procedures []credential.Procedure,
	operator string,
	result bool,
	kid []byte,
	ecdsaPrivateKey *ecdsa.PrivateKey,
	aesKey []byte,
) (qrcode []byte, err error) {
	toEncode, err := CreateBaerCode(
		firstName,
		lastName,
		dob,
		procedures,
		operator,
		result,
		kid,
		ecdsaPrivateKey,
		aesKey,
	)

	if err != nil {
		return
	}

	b64enc := base64.StdEncoding.EncodeToString(toEncode)
	return qr.Create(b64enc, DefaultQRScale)
}

// CreateBaerCode creates the credential, cbors it, encrypts it, signs it and returns the byte array.
func CreateBaerCode(
	firstName string,
	lastName string,
	dob time.Time,
	procedures []credential.Procedure,
	operator string,
	result bool,
	kid []byte,
	ecdsaPrivateKey *ecdsa.PrivateKey,
	aesKey []byte,
) (qrcode []byte, err error) {
	signMessage, err := createUnsignedBaerCode(
		firstName,
		lastName,
		dob,
		procedures,
		operator,
		result,
		kid,
		aesKey,
	)
	if err != nil {
		return
	}

	err = signMessage.SignWithESKey(ecdsaPrivateKey, kid)
	if err != nil {
		return
	}

	signedMessage, err := signMessage.MarshalCBOR()
	if err != nil {
		return nil, err
	}

	toEncode := addVersion(signedMessage)

	return toEncode, nil
}

// createUnsignedBaerCode creates the credential, puts it into an ecrypt0 message, encrypts it,
// and puts it in a signed message envelope and returns it. It does no signing or digest processing.
func createUnsignedBaerCode(
	firstName string,
	lastName string,
	dob time.Time,
	procedures []credential.Procedure,
	operator string,
	result bool,
	kid []byte,
	aesKey []byte,
) (signMessage *cose.SignMessage, err error) {
	cred := credential.New(firstName, lastName, dob, procedures, operator, result)

	cborCred, err := cred.ToCBOR()
	if err != nil {
		return
	}

	em := cose.NewEncrypt0Message(cborCred)
	em.AddKID(kid)
	err = em.EncodeAESGCMMessage(aesKey)
	if err != nil {
		return
	}

	cborEM, err := em.MarshalCBOR()
	if err != nil {
		return
	}

	signMessage = cose.NewSignMessage(cborEM)
	return
}

// CreateUnsignedBaerCodeWithDigests creates the credential, puts in into an encrypt0 message,
// encrypts it, and puts it in a signed message envelope, and creates the signature digests.
// It doesn't sign it, nor does it add the version. The point of this function is to be able to sign the digest
// in a different location.
func CreateUnsignedBaerCodeWithDigests(
	firstName string,
	lastName string,
	dob time.Time,
	procedures []credential.Procedure,
	operator string,
	result bool,
	kid []byte,
	ecdsaPublicKey *ecdsa.PublicKey,
	aesKey []byte,
) (signMessage *cose.SignMessage, digests [][]byte, err error) {
	signMessage, err = createUnsignedBaerCode(
		firstName,
		lastName,
		dob,
		procedures,
		operator,
		result,
		kid,
		aesKey,
	)
	if err != nil {
		return
	}

	digests, err = signMessage.ESSignatureDigests(ecdsaPublicKey, kid)
	if err != nil {
		return
	}

	return
}

// AddSignaturesAndGenerateRawBaerCode takes a SignMessage and its digests, inserts the signatures,
// And returns the raw baercode in bytes.
func AddSignaturesAndGenerateRawBaerCode(
	signMessage *cose.SignMessage,
	signatures [][]byte,
) (baerCode []byte, err error) {
	if len(signMessage.Signatures) != len(signatures) {
		return nil, fmt.Errorf(
			"length of digests don't match up: %d COSE signatures, %d signatures",
			len(signMessage.Signatures),
			len(signatures),
		)
	}

	for i, signature := range signatures {
		signMessage.Signatures[i].SignatureBytes = signature
	}

	cborMessage, err := signMessage.MarshalCBOR()
	if err != nil {
		return
	}

	baerCode = addVersion(cborMessage)

	return baerCode, nil
}

// AddSignaturesAndGenerateQRBaerCode takes a SignMessage and its digests, inserts the signatures,
// And returns the raw baercode in bytes.
func AddSignaturesAndGenerateQRBaerCode(
	signMessage *cose.SignMessage,
	signatures [][]byte,
) (qrcode []byte, err error) {
	rawCode, err := AddSignaturesAndGenerateRawBaerCode(signMessage, signatures)
	if err != nil {
		return
	}

	b64enc := base64.StdEncoding.EncodeToString(rawCode)
	return qr.Create(b64enc, DefaultQRScale)
}

// VerifyBaerCode verifies a baercode. Used mainly for testing right now, so forgive some
// small shortcuts, like expecting the b64 code instead of the qr code.
func VerifyBaerCode(
	b64code string,
	publicKey *ecdsa.PublicKey,
	cryptKey []byte,
) (cred credential.Credential, version uint16, err error) {
	rawCode, err := base64.StdEncoding.DecodeString(b64code)
	if err != nil {
		err = fmt.Errorf("couldn't decode b64 string: %s: %w", b64code, err)
		return
	}
	version, rawSignedMessage := getVersion(rawCode)

	signMessage, err := cose.SignMessageFromCBOR(rawSignedMessage)
	if err != nil {
		return
	}

	var sigErr error
	err = signMessage.VerifyWithESKey(publicKey)
	if err != nil {
		sigErr = fmt.Errorf("couldn't verify message: %w", err)
	}

	em, err := cose.Encrypt0MessageFromCBOR(signMessage.Payload)
	if err != nil {
		return
	}

	err = em.DecodeAESGCMMessage(cryptKey)
	if err != nil {
		return
	}

	cred, err = credential.FromCBOR(em.Payload)
	if err != nil {
		var rawCBOR interface{}
		if cborErr := cbor.Unmarshal(em.Payload, &rawCBOR); cborErr != nil {
			err = fmt.Errorf("couldn't decode as credential: %s, couldn't decode as raw cbor: %w", err, cborErr)
			return
		}
		err = fmt.Errorf("couldn't decode as credential: %w, raw cbor: %+v", err, rawCBOR)
		return
	}

	err = sigErr
	return
}
