module gitlab.com/ponci-berlin/ponci

go 1.16

require (
	github.com/PONCI-Berlin/go-cose v0.0.2
	github.com/boombuler/barcode v1.0.1
	github.com/cenkalti/backoff/v4 v4.1.0
	github.com/fxamacker/cbor/v2 v2.2.1-0.20200429214022-fc263b46c618
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.2.0
	github.com/liyue201/goqr v0.0.0-20200803022322-df443203d4ea
	github.com/minio/minio-go/v7 v7.0.10
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pquerna/otp v1.3.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.8
)
