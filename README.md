# Proof of No Corona infrastructure (PoNCi)

![Pipeline status](https://gitlab.com/ponci-berlin/ponci/badges/main/pipeline.svg)

----

The Proof of No Corona infrastructure, also known as PoNCi, is the backend for the [BärCODE](https://www.baercode.de/)
project, made for Charité and the city of Berlin. It provides a way for COVID-19 test centres and vaccination centres
(which we call issuers) to issue a Proof of No Covid in the form of a QR Code (the BärCODE). The QR Code contains 
only the bare minimum of data, and a cryptographic signature. This BärCODE can then be scanned by a
verifier application, which will parse the data in the BärCODE, verify the signature against a previous downloaded key
bundle. If the signature check passes, the application will verify if the data complies with the rules configured on the
device, and display the name, and the date of birth that is encoded in the BärCODE, so that it can be verified that the
holder of the BärCODE is the person that was tested, or vaccinated, and that the procedure was done in an appropriate
timespan.

The BärCODE project was designed with 3 key goals in mind; privacy, offline verification, and not having a
centralised datastore of people's information. It does this by having the only centralised component be the key management service (KMS), where
approved issuers can submit their public key, which the KMS publishes in a signed bundle, which the verifier can
then download and use it to verify the BärCODES without any need for further internet communication. The information
of BärCODE holders never leaves the issuer infrastructure, except in the form of the BärCODE they receive from
the issuer. Both of these places are places where this data was already readily available. Furthermore, our guidelines
for issuers dictate that keys should be submitted to the KMS the calendar day before they start using them for BärCODE
generation, this way, verifiers can be certain that a bundle downloaded on a certain day will contain the keys for that
day. Of course, because verification happens locally, there is no way to centrally track people via this system.

---

## The contents of this repository

This repository contains the source for the Key Management Service (KMS), the reference issuer, and numerous helper
functions.

## To start developing PoNCi

If you want to start developing on PoNCi, and you want a local KMS or issuer running, follow these guidelines.

### KMS

The KMS requires a working PostgreSQL or CockroachDB instance, an s3 compatible storage solution and a working Go install.

1. Copy the unified example config file from `docs/example_config.yaml` somewhere where you can edit it.
2. Edit the `keyServer`, `webServer`, and `database` sections to reflect your wishes. (The example config has comments
on every setting).
3. Run the KMS `go run ./cmd kms --config /path/to/your/config.yaml`   

### Issuer

The issuer requires a working KMS, and a working Go install.

1. Copy the unified example config file from `docs/example_config.yaml` somewhere where you can edit it.
2. Edit the `issuer`, and `webServer` sections to reflect your wishes. (The example config has comments
 on every setting).
3. Run the KMS `go run ./cmd kms --config /path/to/your/config.yaml` 

## Further information

To read more about BärCODE, and PoNCi, see the following links:

* [Our JS reference verifier implementation](https://gitlab.com/ponci-berlin/js-baercode-verifier/)
* [Our PHP integration library](https://gitlab.com/ponci-berlin/phpbaercode/)
* [BärCODE website](https://www.baercode.de/)
* [The Berlin Senate for Culture and Europe press release](https://www.berlin.de/sen/kulteu/aktuelles/pressemitteilungen/2021/pressemitteilung.1082045.php)
* [The BIH Center for Digital Health press release](https://www.bihealth.org/de/aktuell/baercode-hilft-berlin-bei-rueckkehr-ins-oeffentliche-leben)
