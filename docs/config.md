# covid

## config
You can configure the servers via a yaml file. This yaml file is structured per component, and the configuration parameters
are documented in the `example_config.yaml` in the same directory as this file.
The config file is called ponc.config by default and can be specified with the `--config` parameter.

